public without sharing class MergeLookup {
    @AuraEnabled
    public static List<SObject> findRecords(String jsonObj) {

        LookupWrapper wrap = (LookupWrapper)JSON.deserialize(jsonObj, LookupWrapper.class);

        String key = '%' + wrap.searchKey + '%';
        String QUERY = 'Select Id, '+ wrap.searchField +' From '+ wrap.objectName +' Where '+ wrap.whereClauseField +' LIKE :key';

        if(String.isNotBlank(wrap.filter)){
            QUERY = QUERY+' AND '+ wrap.filter; 
        }      

        List<SObject> sObjectList = Database.query(QUERY);
        List<SObject> modifiedSObjectList = new List<SObject>();

        for(sObject sobj: sObjectList) {

            if(sobj.get('Name') != null && sobj.get('BillingCity') != null) {
                sobj.put('Name', (sobj.get('Name')+','+sobj.get('BillingCity')));
            } else {
                sob.put('Name','');
            }
            modifiedSObjectList.add(sobj);
        }
        return modifiedSObjectList;
    }

    public class LookupWrapper {
        @AuraEnabled public String searchKey {get; set;}
        @AuraEnabled public String objectName {get; set;}
        @AuraEnabled public String searchField {get; set;}
        @AuraEnabled public String whereClauseField {get; set;}
        @AuraEnabled public String filter {get; set;}
    }
}